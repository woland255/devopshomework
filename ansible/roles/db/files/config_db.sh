sec_db_pwd="$1"
#script_name="$0"
sed -i "/\[mysqld\]/atransaction-isolation = READ-COMMITTED\nperformance_schema = on" /etc/mysql/mariadb.conf.d/50-server.cnf
systemctl start mariadb
  #  echo "password $sec_db_pwd" > ~/sec_pwd.txt
  #  echo "Script name $script_name" >> ~/sec_pwd.txt
mysql -u root -e "CREATE DATABASE IF NOT EXISTS owncloud; \
GRANT ALL PRIVILEGES ON owncloud.* \
  TO owncloud@localhost \
  IDENTIFIED BY \"$sec_db_pwd\"";
