occ maintenance:install \
    --database "mysql" \
    --database-name "owncloud" \
    --database-user "owncloud" \
    --database-pass "$1" \
    --data-dir "$2" \
    --admin-user "admin" \
    --admin-pass "$3"
    